package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type User struct {
	Id      string `json:"id"`
	Name    string `json:"name"`
	Balance string `json:"balance"`
}

type MoneyTransfer struct {
	SenderID   string  `json:"senderID"`
	ReceiverID string  `json:"recieverID"`
	Amount     float64 `json:"amount"`
}
type Message struct {
	Mess string
}

var Accounts []User

func readfromjson() (Users []User) {
	byteValue, err := ioutil.ReadFile("./account.json")

	if err != nil {
		fmt.Print(err)
	}

	err = json.Unmarshal(byteValue, &Users)
	if err != nil {
		fmt.Println("error:", err)
	}
	return Users

}

func GetById(id string) (u User) {

	for _, account := range Accounts {
		if account.Id == id {
			u.Id = account.Id
			u.Name = account.Name
			u.Balance = account.Balance
			break
		}
	}
	fmt.Println("All Account is ready to Make Transaction")
	return
}
func EditUserBalance(user User) {
	for i := 0; i < len(Accounts); i++ {

		if Accounts[i].Id == user.Id {

			Accounts[i].Balance = user.Balance

			break
		}
	}

	return
}

func WriteTOJsonFile(fileName string) {

	file, _ := json.MarshalIndent(Accounts, "", " ")

	_ = ioutil.WriteFile(fileName, file, 0644)

}
func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}
func TransfareMoney(w http.ResponseWriter, r *http.Request) {

	reqBody, _ := ioutil.ReadAll(r.Body)

	var transferReq MoneyTransfer
	json.Unmarshal(reqBody, &transferReq)

	var MessageRes Message
	sender := GetById(transferReq.SenderID)
	reciever := GetById(transferReq.ReceiverID)
	if sender.Id == "" || reciever.Id == "" {
		w.WriteHeader(http.StatusInternalServerError)
		MessageRes.Mess = "sender or reciever address not found "
		json.NewEncoder(w).Encode(MessageRes)
		return
	}

	fromAmount, _ := strconv.ParseFloat(sender.Balance, 32)
	toAmount, _ := strconv.ParseFloat(reciever.Balance, 32)
	if fromAmount < transferReq.Amount {
		w.WriteHeader(http.StatusInternalServerError)
		MessageRes.Mess = "your balance not enough"
		json.NewEncoder(w).Encode(MessageRes)
		return
	}

	fromAmount = fromAmount - transferReq.Amount
	toAmount = toAmount + transferReq.Amount
	sender.Balance = fmt.Sprintf("%f", fromAmount)
	reciever.Balance = fmt.Sprintf("%f", toAmount)

	EditUserBalance(sender)
	EditUserBalance(reciever)
	WriteTOJsonFile("./account.json")
	json.NewEncoder(w).Encode(sender)
}
func GetAllAccounts(w http.ResponseWriter, r *http.Request) {

	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(Accounts); err != nil {
		_, _ = fmt.Fprintf(w, "%s", err.Error())
	}
}
func GetAccountsbyId(w http.ResponseWriter, r *http.Request) {
	keys, ok := r.URL.Query()["key"]
	var MessageRes Message
	if !ok || len(keys[0]) < 1 {
		w.WriteHeader(http.StatusInternalServerError)
		MessageRes.Mess = "ID is missing"
		json.NewEncoder(w).Encode(MessageRes)
		return

	}
	id := keys[0]

	fmt.Println("id: ", id)
	user := GetById(id)

	if user.Id == "" {
		w.WriteHeader(http.StatusInternalServerError)
		MessageRes.Mess = "Invalid ID"
		json.NewEncoder(w).Encode(MessageRes)
		return

	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(user)
	return
}
func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/TransfareMoney", TransfareMoney).Methods("POST")
	myRouter.HandleFunc("/GetAllAccounts", GetAllAccounts).Methods("GET")
	myRouter.HandleFunc("/GetAccountsbyId", GetAccountsbyId).Methods("GET")
	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func main() {
	Accounts = readfromjson()
	
	handleRequests()
}
